#!/usr/bin/env bash

# set the ssh key for git interaction
mkdir -p ~/.ssh
(umask 077; echo $SSH_KEY | base64 --decode > ~/.ssh/id_rsa)
cp deploy/ssh_config ~/.ssh/config

# test sshing to bitbucket
ssh -T git@bitbucket.org

# setup GIT
git config --global user.email "pipeline@pep1.com"
git config --global user.name "Pipeline User"

# correct origin settings for correct push
git remote set-url origin git@bitbucket.org:pep1/jira-spring-boot-client.git
cat .git/config

# get the private key for gpg signing and import it
(umask 077; echo $GPG_KEY | base64 --decode > gpg-key.gpg)
gpg --import gpg-key.gpg
rm gpg-key.gpg
