/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.pep1.jira.client;

import com.pep1.jira.client.domain.field.Field;
import com.pep1.jira.client.domain.issue.Attachment;
import com.pep1.jira.client.domain.issue.Comment;
import com.pep1.jira.client.domain.issue.Issue;
import com.pep1.jira.client.domain.issue.IssueSearchResult;
import com.pep1.jira.client.domain.issue.request.IssueInput;
import com.pep1.jira.client.domain.issue.request.IssueSearch;
import com.pep1.jira.client.domain.issue.watcher.WatcherResult;
import com.pep1.jira.client.domain.project.Project;
import com.pep1.jira.client.domain.project.request.ProjectCreation;
import com.pep1.jira.client.domain.user.User;
import com.pep1.jira.client.domain.webhook.Webhook;
import com.pep1.jira.client.domain.webhook.request.WebhookInput;
import com.pep1.jira.client.error.JIRAClientException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * The JIRA Client.
 */
@Slf4j
@Validated
public class JIRAClient {

    /**
     * The JIRA REST API sub path.
     */
    public static final String API_REST_PATH = "rest/api/2";

    /**
     * Webhooks API REST path.
     */
    public static final String WEBHOOKS_REST_PATH = "rest/webhooks/1.0";

    /**
     * The issue cache key.
     */
    public static final String ISSUE_CACHE_KEY = "issues";

    /**
     * the issue search result cache key.
     */
    public static final String ISSUE_SEARCH_CACHE_KEY = "issue_search";

    /**
     * The projects cache key.
     */
    public static final String PROJECTS_CACHE_KEY = "projects";

    /**
     * The user cache key.
     */
    public static final String USERS_CACHE_KEY = "users";

    /**
     * The instance fields cache key.
     */
    public static final String FIELDS_CACHE_KEY = "fields";

    /**
     * The resource paths.
     */
    public final class Path {
        public static final String PROJECT = "project";
        public static final String ISSUE = "issue";
        public static final String SEARCH = "search";
        public static final String USERS = "user";
        public static final String WORKLOG = "worklog";
        public static final String ISSUETYPE = "issuetype";
        public static final String PRIORITY = "priority";
        public static final String ATTACHMENT = "attachments";
        public static final String WATCHERS = "watchers";
        public static final String FIELD = "field";
        public static final String COMMENT = "comment";
    }

    /**
     * The rest template to use.
     */
    private final RestTemplate restTemplate;

    /**
     * The JIRA instance base URI.
     */
    private final URI baseUri;

    /**
     * The base URI containing {@link JIRAClient#API_REST_PATH} used for most rest requests.
     */
    private final URI apiBaseUri;

    public JIRAClient(@NonNull URI baseUri, @NonNull RestTemplate restTemplate) {
        this.baseUri = baseUri;
        this.restTemplate = restTemplate;
        this.apiBaseUri = UriComponentsBuilder.fromUri(baseUri).pathSegment(API_REST_PATH).build().toUri();
    }

    private UriComponentsBuilder basePath() {
        return UriComponentsBuilder.fromUri(apiBaseUri);
    }

    /**
     * Fetches an issue by the given key.
     *
     * @param key the issue key
     * @return the issue
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @Cacheable(cacheNames = ISSUE_CACHE_KEY, key = "#key")
    public Issue getIssue(@NonNull String key) throws JIRAClientException {
        return restTemplate.getForObject(
                basePath().pathSegment(Path.ISSUE, key).build().toUri(),
                Issue.class
        );
    }

    /**
     * Fetches an issue by the given key.
     * If <code>forceUpdate</code> is true, the cache will be bypassed and refreshed.
     *
     * @param key         the issue key
     * @param forceUpdate overrides the cache if true
     * @return the created issue
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @Cacheable(cacheNames = ISSUE_CACHE_KEY, key = "#key", condition = "#forceUpdate != true")
    public Issue getIssue(@NonNull String key, Boolean forceUpdate) throws JIRAClientException {
        return getIssue(key);
    }

    /**
     * Gets the watching users of an issue.
     *
     * @param issueKey the issue key
     * @return the result list of watchers
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    public WatcherResult getWatchers(@NonNull String issueKey) {
        return restTemplate.getForObject(
                basePath().pathSegment(Path.ISSUE, issueKey, Path.WATCHERS).build().toUri(),
                WatcherResult.class
        );
    }

    /**
     * Creates a new issue.
     *
     * @param issueInput the issue creation request
     * @return the created issue
     */
    @CachePut(cacheNames = ISSUE_CACHE_KEY, key = "#result.key")
    public Issue createIssue(@NonNull @Valid IssueInput issueInput) throws JIRAClientException {
        Issue created = restTemplate.postForObject(
                basePath().pathSegment(Path.ISSUE).build().toUri(),
                issueInput,
                Issue.class
        );

        return getIssue(created.getKey());
    }

    /**
     * Updates an existing issue by the given input.
     *
     * @param issueInput the issue fields to update
     * @return issue the updated issue
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @CacheEvict(cacheNames = ISSUE_CACHE_KEY, key = "#key")
    public Issue updateIssue(@NonNull String key, @NonNull @Valid IssueInput issueInput)
            throws JIRAClientException {
        restTemplate.exchange(
                basePath().pathSegment(Path.ISSUE, key).build().toUri(),
                HttpMethod.PUT,
                new HttpEntity<IssueInput>(issueInput),
                Issue.class
        );

        return getIssue(key);
    }

    /**
     * Adds an attachment to the issue specified by the given id.
     *
     * @param key      of the issue to add the attachment to
     * @param resource the resource file
     * @return all attachments of the issue, including the new one
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @CacheEvict(cacheNames = ISSUE_CACHE_KEY, key = "#key")
    public List<Attachment> addAttachment(@NonNull String key, @NonNull Resource resource)
            throws JIRAClientException {
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", resource);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity =
                new HttpEntity<LinkedMultiValueMap<String, Object>>(map, headers);

        return restTemplate.exchange(
                basePath().pathSegment(Path.ISSUE, key, Path.ATTACHMENT).build().toUri(),
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<List<Attachment>>() {
                }
        ).getBody();
    }

    /**
     * Adds an comment to the issue specified by the given id.
     *
     *  @param key of the issue to add the comment to
     *  @param comment the comment creation request
     *  @return the created comment
     *
     */
    public Comment addComment(@NonNull String key, @NonNull Comment comment) throws JIRAClientException {
        Comment created =  this.restTemplate.postForObject(this.basePath()
                .pathSegment(new String[] { Path.ISSUE, key })
                .pathSegment(new String[] { Path.COMMENT })
                .build()
                .toUri(), comment, Comment.class);

        return created;
    }

    /**
     * Returns a paged resource by the given search request.
     *
     * @param issueSearch the search request
     * @return the search result
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @Cacheable(cacheNames = ISSUE_SEARCH_CACHE_KEY, key = "#issueSearch.hashCode().toString()")
    public IssueSearchResult searchIssues(@NonNull @Valid IssueSearch issueSearch) throws JIRAClientException {
        return restTemplate.postForObject(
                basePath().pathSegment(Path.SEARCH).build().toUri(),
                issueSearch,
                IssueSearchResult.class
        );
    }

    /**
     * Fetches a project by the given project key.
     *
     * @param projectKey the project key
     * @return the project
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @Cacheable(cacheNames = PROJECTS_CACHE_KEY, key = "#projectKey")
    public Project getProject(@NonNull String projectKey) throws JIRAClientException {
        return restTemplate.getForObject(
                basePath().pathSegment(Path.PROJECT, projectKey).build().toUri(),
                Project.class
        );
    }

    /**
     * Creates a new project.
     *
     * @param projectCreation the project creation request
     * @return the created project
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    public Project createProject(@NonNull @Valid ProjectCreation projectCreation) throws JIRAClientException {
        return restTemplate.postForObject(
                basePath().pathSegment(Path.PROJECT).build().toUri(),
                projectCreation,
                Project.class
        );
    }

    /**
     * Checks if a project with the given key exists.
     *
     * @param projectKey the project key
     * @return true if one exists, false otherwise
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    public Boolean projectExists(@NonNull String projectKey) throws JIRAClientException {
        try {
            return (getProject(projectKey) != null);
        } catch (JIRAClientException e) {
            if (HttpStatus.NOT_FOUND.equals(e.getStatus())) {
                log.info("Project with id {} not found", projectKey);
                return false;
            }

            throw e;
        }
    }

    /**
     * Deletes a project specified by the given key.
     * Silently ignores a not existing project.
     *
     * @param projectKey the project key
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @CacheEvict(cacheNames = PROJECTS_CACHE_KEY, key = "#projectKey")
    public void deleteProject(@NonNull String projectKey) throws JIRAClientException {
        try {
            restTemplate.delete(basePath().pathSegment(Path.PROJECT, projectKey).build().toUri());
        } catch (JIRAClientException e) {
            if (HttpStatus.NOT_FOUND.equals(e.getStatus())) {
                log.warn("Project with id '{}' not found for deletion, ignoring", projectKey);
                return;
            }
            throw e;
        }
    }

    /**
     * Gets a user object by given user name.
     *
     * @param instanceId the instance id
     * @param username   the username or email
     * @return the found user
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @Cacheable(cacheNames = USERS_CACHE_KEY, key = "#instanceId.concat('-').concat(#username)")
    public User findFirstUser(@NonNull String instanceId, @NonNull String username) {
        List<User> result = restTemplate.exchange(
                basePath().pathSegment(Path.USERS, Path.SEARCH)
                        .queryParam("username", username)
                        .build().toUri(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<User>>() {
                }
        ).getBody();

        return (result.isEmpty()) ? null : result.get(0);
    }

    /**
     * Creates a new webhook on the target JIRA instance.
     *
     * @param request a webhook creation request
     * @return the created webhook object
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    public Webhook createWebhook(@NonNull @Valid WebhookInput request) throws JIRAClientException {
        return restTemplate.postForObject(
                UriComponentsBuilder.fromUri(baseUri).pathSegment(WEBHOOKS_REST_PATH, "webhook").build().toUri(),
                request,
                Webhook.class
        );
    }

    /**
     * Fetches an existing webhook by id.
     *
     * @param id the given id
     * @return the webhook object
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    public Webhook getWebhook(@NonNull String id) throws JIRAClientException {
        return restTemplate.getForObject(
                UriComponentsBuilder.fromUri(baseUri).pathSegment(WEBHOOKS_REST_PATH, "webhook", id).build().toUri(),
                Webhook.class
        );
    }

    /**
     * Fetches all existing webhook in the target JIRA instance.
     *
     * @return the list of webhooks
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    public List<Webhook> listWebhooks() throws JIRAClientException {
        return restTemplate.exchange(
                UriComponentsBuilder.fromUri(baseUri).pathSegment(WEBHOOKS_REST_PATH, "webhook").build().toUri(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Webhook>>() {
                }
        ).getBody();
    }

    /**
     * Deletes a webhook identified by the given id.
     * Silently ignores a non existent webhook.
     *
     * @param id the given id
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    public void deleteWebhook(@NonNull String id) throws JIRAClientException {
        try {
            restTemplate.delete(
                    UriComponentsBuilder.fromUri(baseUri).pathSegment(WEBHOOKS_REST_PATH, "webhook", id).build().toUri()
            );
        } catch (JIRAClientException e) {
            if (HttpStatus.NOT_FOUND.equals(e.getStatus())) {
                log.warn("Webhook with id '{}' not found for deletion, ignoring", id);
                return;
            }
            throw e;
        }
    }

    /**
     * Fetches all existing fields in the target JIRA instance.
     *
     * @return the list of fields
     * @throws JIRAClientException if an error response is returned from JIRA
     */
    @Cacheable(cacheNames = FIELDS_CACHE_KEY)
    public List<Field> listFields() throws JIRAClientException {
        return restTemplate.exchange(
                UriComponentsBuilder.fromUri(baseUri).pathSegment(API_REST_PATH, Path.FIELD).build().toUri(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Field>>() {
                }
        ).getBody();
    }
}
