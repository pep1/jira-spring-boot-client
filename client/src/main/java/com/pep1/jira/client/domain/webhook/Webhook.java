/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.pep1.jira.client.domain.webhook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pep1.jira.client.domain.DTO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * For details, please see https://developer.atlassian.com/jiradev/jira-apis/webhooks#Webhooks-rest
 */
@Data
@Slf4j
public class Webhook implements DTO {

    public static final String ID_NOT_SET = "not_set";

    private String self;

    private String name;
    private String url;
    private Set<String> events;
    private Map<String, String> filters;
    private Boolean enabled;

    private String lastUpdatedUser;
    private String lastUpdatedDisplayName;
    private DateTime lastUpdated;

    @JsonIgnore
    public String getId() {
        if (self == null) {
            return ID_NOT_SET;
        }

        try {
            List<String> pathSegments = UriComponentsBuilder.fromUriString(self).build().getPathSegments();

            if (!pathSegments.isEmpty()) {
                return pathSegments.get(pathSegments.size() - 1);
            }
        } catch (IllegalArgumentException e) {
            log.warn("Not a valid URL", e);
        }

        return ID_NOT_SET;
    }
}
