/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.pep1.jira.client.domain.issue;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.pep1.jira.client.domain.DTO;
import com.pep1.jira.client.domain.project.Project;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize(builder = IssueFields.IssueFieldsBuilder.class)
public class IssueFields implements DTO {

    @JsonPOJOBuilder(withPrefix = "")
    public static final class IssueFieldsBuilder {
        // workaround for https://github.com/peichhorn/lombok-pg/issues/141
        @JsonIgnore
        private Map<String, Object> customFields = new HashMap<String, Object>();

        @JsonAnyGetter
        public Map<String, Object> customFields() {
            return customFields;
        }

        @JsonAnySetter
        public void customFields(String key, Object value) {
            if (key == null || value == null) {
                return;
            }
            customFields.put(key, value);
        }
    }

    private Project project;
    private String summary;

    private IssueType issuetype;

    private Reporter creator;
    private Reporter assignee;
    private Reporter reporter;

    private Priority priority;

    private DateTime created;
    private DateTime updated;
    private DateTime dueDate;
    private DateTime resolutionDate;

    private Map<String, String> progress;
    private TimeTracking timetracking;

    private String timeSpent;
    private String description;

    private String[] issueLinks;
    private String[] subTasks;
    private Status status;
    private String[] labels;
    private Integer workRatio;
    private String environment;
    private List<Component> components;
    private CommentList comment;
    private Vote votes;
    private Resolution resolution;
    private String[] fixVersions;
    private Map<String, String> watches;
    private Worklog worklog;
    private List<Attachment> attachment;
    private List<Version> versions;
    private DateTime timeEstimate;

    // for custom field
    @JsonIgnore
    private Map<String, Object> customFields = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getCustomfields() {
        return customFields;
    }

    @JsonAnySetter
    public void setCustomfield(String key, Object value) {
        if (key == null || value == null) {
            return;
        }
        customFields.put(key, value);
    }

    // Helper methods
    public IssueFields setProjectId(String id) {
        if (project == null)
            project = new Project();

        project.setId(id);

        return this;
    }

    public IssueFields setProjectKey(String key) {
        if (project == null)
            project = new Project();

        project.setKey(key);

        return this;
    }

    public IssueFields setIssueTypeId(String id) {
        if (issuetype == null)
            issuetype = new IssueType();
        issuetype.setId(id);

        return this;
    }

    public IssueFields setIssueTypeName(String name) {
        if (issuetype == null)
            issuetype = new IssueType();
        issuetype.setName(name);

        return this;
    }

    public IssueFields setAssigneeName(String name) {
        if (assignee == null)
            assignee = new Reporter();

        assignee.setName(name);

        return this;
    }

    public IssueFields setReporterName(String name) {
        if (reporter == null)
            reporter = new Reporter();

        reporter.setName(name);

        return this;
    }

    public IssueFields setPriorityId(String id) {
        if (priority == null)
            priority = new Priority();

        priority.setId(id);

        return this;
    }

    public IssueFields setPriorityName(String name) {
        if (priority == null)
            priority = new Priority();

        priority.setName(name);

        return this;
    }
}
