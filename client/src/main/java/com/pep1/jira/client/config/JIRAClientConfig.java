/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.pep1.jira.client.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.pep1.jira.client.JIRAClient;
import com.pep1.jira.client.JIRAHeaderInterceptor;
import com.pep1.jira.client.error.JIRAErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import java.util.TimeZone;

/**
 * Default JIRA Client auto configuration.
 */
@ConditionalOnProperty("jira.enabled")
@Configuration
@Import(ValidationConfig.class)
@EnableConfigurationProperties(JIRAClientProperties.class)
public class JIRAClientConfig {

    public static final String JIRA_AUTH_INTERCEPTOR_BEAN_NAME = "jiraAuthInterceptor";
    public static final String JIRA_OBJECT_MAPPER_BEAN_NAME = "jiraObjectMapper";
    public static final String JIRA_REST_TEMPLATE_BEAN_NAME = "jiraRestTemplate";

    private final JIRAClientProperties properties;

    @Autowired
    public JIRAClientConfig(JIRAClientProperties properties) {
        this.properties = properties;
    }

    /**
     * The actual JIRA client bean.
     *
     * @param restTemplate to use, should be pre-configured using appropriate authentication
     * @return the JIRA client
     */
    @Bean
    public JIRAClient jiraClient(@Qualifier(JIRA_REST_TEMPLATE_BEAN_NAME) RestTemplate restTemplate) {
        return new JIRAClient(properties.getInstance().getUri(), restTemplate);
    }

    /**
     * The rest template to use for the JIRA client.
     *
     * @param authInterceptor to use to authenticate against the JIRA instance
     * @param mapper          the Jackson object mapper for internal mapping
     * @return the rest template
     */
    @Bean(JIRA_REST_TEMPLATE_BEAN_NAME)
    @ConditionalOnMissingBean(name = JIRA_REST_TEMPLATE_BEAN_NAME)
    public RestTemplate jiraRestTemplate(
            @Qualifier(JIRA_AUTH_INTERCEPTOR_BEAN_NAME) ClientHttpRequestInterceptor authInterceptor,
            @Qualifier(JIRA_OBJECT_MAPPER_BEAN_NAME) ObjectMapper mapper
    ) {
        return new RestTemplateBuilder()
                .setReadTimeout(properties.getInstance().getReadTimeout())
                .setConnectTimeout(properties.getInstance().getConnectionTimeout())
                .messageConverters(
                        // for multipart/form-data (attachments)
                        new FormHttpMessageConverter(),
                        // for json
                        new MappingJackson2HttpMessageConverter(mapper)
                )
                .additionalInterceptors(
                        authInterceptor,
                        new JIRAHeaderInterceptor()
                )
                .errorHandler(new JIRAErrorHandler(mapper))
                .build();
    }

    /**
     * The Jackson object mapper configured to work with the default JIRA REST API.
     *
     * @return the object mapper
     */
    @Bean(JIRA_OBJECT_MAPPER_BEAN_NAME)
    @ConditionalOnMissingBean(name = JIRA_OBJECT_MAPPER_BEAN_NAME)
    public ObjectMapper jiraObjectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .failOnEmptyBeans(false)
                .failOnUnknownProperties(false)
                .dateFormat(new ISO8601DateFormat())
                .timeZone(TimeZone.getTimeZone("UTC"))
                .modulesToInstall(new JodaModule())
                .build();
    }

    /**
     * The default basic authorization interceptor to authenticate against a JIRA instance.
     *
     * @return the auth interceptor
     */
    @Bean(JIRA_AUTH_INTERCEPTOR_BEAN_NAME)
    @ConditionalOnMissingBean(name = JIRA_AUTH_INTERCEPTOR_BEAN_NAME)
    public ClientHttpRequestInterceptor basicAuthorizationInterceptor() {
        Assert.hasText(properties.getInstance().getUsername(), "A username should be specified");
        Assert.hasText(properties.getInstance().getPassword(), "A password should be specified");

        return new BasicAuthorizationInterceptor(
                properties.getInstance().getUsername(),
                properties.getInstance().getPassword()
        );
    }
}
