/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.pep1.jira.client.domain.issue;

import com.pep1.jira.client.domain.DTO;
import com.pep1.jira.client.domain.user.User;
import lombok.Data;

@Data
public class WorklogElement implements DTO, Comparable<WorklogElement> {

    private static final String TAG = WorklogElement.class.getName();

    private User updateAuthor;
    private Integer timeSpentSeconds;
    private String started;
    private String self;
    private String comment;
    private String timeSpent;
    private User author;
    private String id;
    private String created;
    private String updated;

    @Override
    public int compareTo( WorklogElement o) {
        final String idString = this.id;
        final String idStringOther = o.id;

        final Integer currentId = Integer.valueOf(idString);
        final Integer outID = Integer.valueOf(idStringOther);

        return currentId.compareTo(outID);

    }
}
