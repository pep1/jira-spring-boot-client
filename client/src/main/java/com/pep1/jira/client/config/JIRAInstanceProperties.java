/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.pep1.jira.client.config;

import lombok.Data;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;

@Data
public class JIRAInstanceProperties {

    /**
     * The username to authenticate.
     */
    private String username = "admin";

    /**
     * The password to authenticate
     */
    private String password = "admin";

    /**
     * The JIRA uri including the sub path (e.g. http://localhost:8080/jira)
     */
    private URI uri;

    /**
     * The Client connection timeout.
     */
    private Duration connectionTimeout = Duration.ofSeconds(5);

    /**
     * The Client read timeout.
     */
    private Duration readTimeout = Duration.ofSeconds(5);

    public JIRAInstanceProperties() {
        try {
            // workaround to set default value with catching syntax exception
            uri = new URI("http://localhost:8080");
        } catch (URISyntaxException e) {
            // this should not happen
        }
    }
}
