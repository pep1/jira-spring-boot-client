/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.pep1.jira.client.domain.webhook.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pep1.jira.client.domain.DTO;
import com.pep1.jira.client.domain.issue.Comment;
import com.pep1.jira.client.domain.issue.Issue;
import com.pep1.jira.client.domain.issue.changelog.Changelog;
import com.pep1.jira.client.domain.user.User;
import lombok.Data;
import org.joda.time.DateTime;

@Data
public class Event implements DTO {
    private DateTime timestamp;
    @JsonProperty("webhookEvent")
    private EventType type;
    @JsonProperty("issue_event_type_name")
    private String eventType;
    private User user;
    private Issue issue;
    private Comment comment;
    private Changelog changelog;
    private InstanceDTO instance;
}
