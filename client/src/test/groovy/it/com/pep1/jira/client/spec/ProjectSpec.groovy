/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package it.com.pep1.jira.client.spec

import com.pep1.jira.client.JIRAClient
import com.pep1.jira.client.domain.project.Project
import com.pep1.jira.client.domain.project.request.ProjectCreation
import it.com.pep1.jira.client.BaseSpecification
import org.springframework.beans.factory.annotation.Autowired

class ProjectSpec extends BaseSpecification {

    static projectKey = "PRTEST"

    @Autowired
    JIRAClient client

    def "Should be able to create test project"() {
        setup:
        "Make sure no project with key '$projectKey' exists"
        client.deleteProject(projectKey)

        when:
        "Creating a project with key '$projectKey'"
        client.createProject(new ProjectCreation(
                key: projectKey,
                name: "An issue test project",
                lead: "admin",
                projectTypeKey: "business"
        ))
        Project project = client.getProject(projectKey)

        then: "New project should have an id and the same key"
        project.id != null
        project.key == projectKey

        cleanup: "Cleaning up created project"
        client.deleteProject(projectKey)
    }
}
