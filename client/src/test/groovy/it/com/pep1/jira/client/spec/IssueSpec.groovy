/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package it.com.pep1.jira.client.spec

import com.pep1.jira.client.JIRAClient
import com.pep1.jira.client.domain.issue.Issue
import com.pep1.jira.client.domain.issue.IssueFields
import com.pep1.jira.client.domain.issue.IssueSearchResult
import com.pep1.jira.client.domain.issue.IssueType
import com.pep1.jira.client.domain.issue.request.IssueInput
import com.pep1.jira.client.domain.issue.request.IssueSearch
import com.pep1.jira.client.domain.project.request.ProjectCreation
import com.pep1.jira.client.error.JIRAClientException
import it.com.pep1.jira.client.BaseSpecification
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Stepwise
import spock.lang.Unroll

@Stepwise
class IssueSpec extends BaseSpecification {

    static final projectKey = "ISSUETEST"

    static final goodIssueCreations = [
            new IssueInput(
                    fields: new IssueFields(
                            projectKey: projectKey,
                            issueTypeName: IssueType.TASK,
                            summary: "This is a test task"
                    )
            ),
            new IssueInput(
                    fields: new IssueFields(
                            projectKey: projectKey,
                            issueTypeName: IssueType.TASK,
                            summary: "Another one, forgot it"
                    )
            ),
            new IssueInput(
                    fields: new IssueFields(
                            projectKey: projectKey,
                            issueTypeName: IssueType.TASK,
                            summary: "This is an improvement"
                    )
            )
    ]

    static final badIssueCreations = [
            new IssueInput(
                    fields: new IssueFields(
                            projectKey: "not_existent_project",
                            issueTypeName: IssueType.TASK,
                            summary: "This is a test task"
                    )
            ),
            new IssueInput(
                    fields: new IssueFields(
                            projectKey: projectKey,
                            issueTypeName: "i_am_not_there",
                            summary: "Not existent issue type"
                    )
            ),
            new IssueInput(
                    fields: new IssueFields(
                            projectKey: projectKey,
                            issueTypeName: IssueType.TASK,
                            summary: null
                    )
            )
    ]

    static final goodIssueUpdates = [
            new IssueInput(
                    fields: new IssueFields(
                            assigneeName: "admin"
                    )
            ),
            new IssueInput(
                    fields: new IssueFields(
                            description: "Some updated issue description"
                    )
            )
    ]

    @Autowired
    JIRAClient client

    def "Create test project"() {
        when: "creating the test project"
        if (!client.projectExists(projectKey)) {
            client.createProject(new ProjectCreation(
                    key: projectKey,
                    name: "An issue test project",
                    lead: "admin",
                    projectTypeKey: "business"
            ))
        }

        then: "the test project should exist"
        client.projectExists(projectKey)
    }

    @Unroll
    "Should be able to create issue '#issueInput.fields.summary'"(IssueInput issueInput) {
        when: "the issue create request is sent"
        Issue issue = client.createIssue(issueInput)

        then: "the issue is created and has an id"
        issue != null
        issue.id != null

        where: "some good issue creations are sent"
        issueInput << goodIssueCreations
    }

    @Unroll
    "Should throw exception when trying to create '#issueInput.fields.summary'"(IssueInput issueInput) {
        when: "the issue create request is sent"
        client.createIssue(issueInput)

        then: "an exception is thrown"
        thrown JIRAClientException

        where: "some bad issue creations are sent"
        issueInput << badIssueCreations
    }

    @Unroll
    "Should be able to update issue with '#issueInput.toString()' "(IssueInput issueInput) {
        given: "there is an issue present"
        Issue issue = client.createIssue(goodIssueCreations.get(0))

        when: "the issue update request is sent"
        Issue updatedIssue = client.updateIssue(issue.key, issueInput)

        then: "the issue is updated correctly"
        updatedIssue != null
        updatedIssue.key == issue.key

        where: "some good issue update requests are sent"
        issueInput << goodIssueUpdates
    }

    def "Should be able to search issues"() {
        when: "the search request is sent"
        IssueSearchResult result = client.searchIssues(new IssueSearch(
                jql: "project = $projectKey",
                maxResults: 20
        ))

        then: "it returns a search result"
        result != null
        result.issues.size() > 0
    }

    def "Cleanup test project"() {
        when: "sending the project delete request"
        if (client.projectExists(projectKey)) {
            client.deleteProject(projectKey)
        }

        then: "the test project should not exists anymore"
        !client.projectExists(projectKey)
    }
}
