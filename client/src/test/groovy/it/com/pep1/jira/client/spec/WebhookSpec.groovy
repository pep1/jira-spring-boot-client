/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package it.com.pep1.jira.client.spec

import com.pep1.jira.client.JIRAClient
import com.pep1.jira.client.domain.webhook.Webhook
import com.pep1.jira.client.domain.webhook.event.EventType
import com.pep1.jira.client.domain.webhook.request.WebhookInput
import it.com.pep1.jira.client.BaseSpecification
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Stepwise
import spock.lang.Unroll


@Stepwise
class WebhookSpec extends BaseSpecification {

    @Autowired
    JIRAClient client

    static final goodWebhookCreations = [
            new WebhookInput(
                    name: "test webhook",
                    excludeIssueDetails: false,
                    url: "http://example.com",
                    events: [EventType.ISSUE_CREATED]
            ),
            new WebhookInput(
                    name: "test2 webhook",
                    excludeIssueDetails: true,
                    url: "http://pep1.com",
                    events: [EventType.ISSUE_DELETED]
            ),
            new WebhookInput(
                    name: "test3 webhook",
                    excludeIssueDetails: false,
                    url: "https://atlassian.com",
                    events: [EventType.ISSUE_UPDATED]
            )
    ]

    @Unroll
    "Should be able to create a '#input.name'"(WebhookInput input) {
        when: "a webhook create request is sent"
        Webhook webhook = client.createWebhook(input)

        then: "webhook is created"
        webhook != null
        webhook.id != Webhook.ID_NOT_SET

        cleanup: "cleanup the created webhook"
        client.deleteWebhook(webhook.id)

        where: "some good webhook create data"
        input << goodWebhookCreations
    }

    def "Should get an existing webhook"() {
        given: "a webhook exists"
        Webhook existingWebhook = client.createWebhook(goodWebhookCreations.get(0))
        expect: "webhook can be fetched"
        Webhook webhook = client.getWebhook(existingWebhook.id)

        webhook != null
        webhook.id != Webhook.ID_NOT_SET
        webhook.name == existingWebhook.name

        cleanup: "cleanup the created webhook"
        client.deleteWebhook(webhook.id)
    }

    def "Should get all existing webhooks"() {
        given: "some webhooks exist"
        List<Webhook> existing = new ArrayList<>()
        goodWebhookCreations.each { input ->
            existing.add(client.createWebhook(input))
        }

        when: "a list webhook request is done"
        List<Webhook> loaded = client.listWebhooks()

        then: "All existing are listed"
        loaded.containsAll(existing)

        cleanup: "cleanup the created webhook"
        existing.each { webhook ->
            client.deleteWebhook(webhook.id)
        }
    }
}
