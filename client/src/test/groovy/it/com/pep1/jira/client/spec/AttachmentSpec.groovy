/*
 * MIT License
 *
 * Copyright (c) 2019 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package it.com.pep1.jira.client.spec

import com.pep1.jira.client.JIRAClient
import com.pep1.jira.client.domain.issue.Attachment
import com.pep1.jira.client.domain.issue.Issue
import com.pep1.jira.client.domain.issue.IssueFields
import com.pep1.jira.client.domain.issue.IssueType
import com.pep1.jira.client.domain.issue.request.IssueInput
import com.pep1.jira.client.domain.project.request.ProjectCreation
import it.com.pep1.jira.client.BaseSpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import spock.lang.Stepwise


@Stepwise
class AttachmentSpec extends BaseSpecification {

    static final projectKey = "ATTACHTEST"

    @Autowired
    JIRAClient client

    def "Create test project"() {
        when: "creating the test project"
        if (!client.projectExists(projectKey)) {
            client.createProject(new ProjectCreation(
                    key: projectKey,
                    name: "An issue test project",
                    lead: "admin",
                    projectTypeKey: "business"
            ))
        }

        then: "the test project should exist"
        client.projectExists(projectKey)
    }

    def "Should create an issue and add attachment"() {
        when: "an issue is created and an attachment is uploaded"
        Issue issue = client.createIssue(new IssueInput(
                fields: new IssueFields(
                        projectKey: projectKey,
                        issueTypeName: IssueType.TASK,
                        summary: "This is a test task"
                )
        ))

        List<Attachment> attachmentList = client.addAttachment(
                issue.key,
                new ClassPathResource("test-resource/testfile.md")
        )

        then: "attachment is created and in the return list"
        attachmentList.size() > 0
        attachmentList.find { Attachment attachment -> attachment.filename == "testfile.md" }
    }

    def "Cleanup test project"() {
        when: "sending the project delete request"
        client.deleteProject(projectKey)

        then: "the test project should not exists anymore"
        !client.projectExists(projectKey)
    }
}
